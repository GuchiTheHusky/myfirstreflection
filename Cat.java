package Reflection;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Cat {
    public static void main(String[] args) throws IllegalAccessException, RuntimeException {
        Animal animal = new Animal();
        Class<?> cls = animal.getClass();
        Field[] fields = cls.getDeclaredFields();

        System.out.println("<<<" + cls + ">>>" + "\n" + "Fields and types: " + Arrays.toString(fields) + "\n");
        animalReflection(animal, "Axel", 4, false);
    }

    private static void animalReflection(Animal animal, String name, Integer age, Boolean isHungry) throws IllegalAccessException, RuntimeException {
        Class<? extends Animal> aClass = animal.getClass();
        for (Field field : aClass.getDeclaredFields()) {
            if ("name".equals(field.getName())) {
                field.setAccessible(true);
                Object oldValue = field.get(animal);
                field.set(animal, name);
                System.out.println("We changed old value: " + oldValue + " to " + name);
            }
            if ("age".equals(field.getName())) {
                field.setAccessible(true);
                Object oldValue1 = field.get(animal);
                field.set(animal, age);
                System.out.println("We changed old value: " + oldValue1 + " to " + age);
            }
            if ("isHungry".equals(field.getName())) {
                field.setAccessible(true);
                Object oldValue = field.get(animal);
                field.set(animal, isHungry);
                System.out.println("We changed old value: " + oldValue + " to " + isHungry);
            }
        }
    }
}
